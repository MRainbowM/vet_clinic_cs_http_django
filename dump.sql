-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: vc_3
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can view log entry',1,'view_logentry'),(5,'Can add permission',2,'add_permission'),(6,'Can change permission',2,'change_permission'),(7,'Can delete permission',2,'delete_permission'),(8,'Can view permission',2,'view_permission'),(9,'Can add group',3,'add_group'),(10,'Can change group',3,'change_group'),(11,'Can delete group',3,'delete_group'),(12,'Can view group',3,'view_group'),(13,'Can add user',4,'add_user'),(14,'Can change user',4,'change_user'),(15,'Can delete user',4,'delete_user'),(16,'Can view user',4,'view_user'),(17,'Can add content type',5,'add_contenttype'),(18,'Can change content type',5,'change_contenttype'),(19,'Can delete content type',5,'delete_contenttype'),(20,'Can view content type',5,'view_contenttype'),(21,'Can add session',6,'add_session'),(22,'Can change session',6,'change_session'),(23,'Can delete session',6,'delete_session'),(24,'Can view session',6,'view_session'),(25,'Can add Вид животного',7,'add_kind'),(26,'Can change Вид животного',7,'change_kind'),(27,'Can delete Вид животного',7,'delete_kind'),(28,'Can view Вид животного',7,'view_kind'),(29,'Can add Клиент',8,'add_client'),(30,'Can change Клиент',8,'change_client'),(31,'Can delete Клиент',8,'delete_client'),(32,'Can view Клиент',8,'view_client'),(33,'Can add Питомец',9,'add_pet'),(34,'Can change Питомец',9,'change_pet'),(35,'Can delete Питомец',9,'delete_pet'),(36,'Can view Питомец',9,'view_pet'),(37,'Can add user',10,'add_user'),(38,'Can change user',10,'change_user'),(39,'Can delete user',10,'delete_user'),(40,'Can view user',10,'view_user'),(41,'Can add Прием',11,'add_visit'),(42,'Can change Прием',11,'change_visit'),(43,'Can delete Прием',11,'delete_visit'),(44,'Can view Прием',11,'view_visit'),(45,'Can add Должность',12,'add_position'),(46,'Can change Должность',12,'change_position'),(47,'Can delete Должность',12,'delete_position'),(48,'Can view Должность',12,'view_position'),(49,'Can add Работник',13,'add_worker'),(50,'Can change Работник',13,'change_worker'),(51,'Can delete Работник',13,'delete_worker'),(52,'Can view Работник',13,'view_worker'),(53,'Can add Услуга',14,'add_service'),(54,'Can change Услуга',14,'change_service'),(55,'Can delete Услуга',14,'delete_service'),(56,'Can view Услуга',14,'view_service'),(57,'Can add Направление',15,'add_direction'),(58,'Can change Направление',15,'change_direction'),(59,'Can delete Направление',15,'delete_direction'),(60,'Can view Направление',15,'view_direction'),(61,'Can add Филиал',16,'add_filial'),(62,'Can change Филиал',16,'change_filial'),(63,'Can delete Филиал',16,'delete_filial'),(64,'Can view Филиал',16,'view_filial'),(65,'Can add Кабинет',17,'add_cabinet'),(66,'Can change Кабинет',17,'change_cabinet'),(67,'Can delete Кабинет',17,'delete_cabinet'),(68,'Can view Кабинет',17,'view_cabinet'),(69,'Can add schedule',18,'add_schedule'),(70,'Can change schedule',18,'change_schedule'),(71,'Can delete schedule',18,'delete_schedule'),(72,'Can view schedule',18,'view_schedule');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clients_client`
--

DROP TABLE IF EXISTS `clients_client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clients_client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `surname` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `patronymic` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `mail` varchar(50) DEFAULT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `date_of_delete` varchar(20) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `clients_client_user_id_57c8fc4a_fk_users_user_id` (`user_id`),
  CONSTRAINT `clients_client_user_id_57c8fc4a_fk_users_user_id` FOREIGN KEY (`user_id`) REFERENCES `users_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clients_client`
--

LOCK TABLES `clients_client` WRITE;
/*!40000 ALTER TABLE `clients_client` DISABLE KEYS */;
INSERT INTO `clients_client` VALUES (1,'Иванов','Иван','Иванович',NULL,NULL,'',NULL,NULL,1),(2,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,3);
/*!40000 ALTER TABLE `clients_client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clients_kind`
--

DROP TABLE IF EXISTS `clients_kind`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clients_kind` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clients_kind`
--

LOCK TABLES `clients_kind` WRITE;
/*!40000 ALTER TABLE `clients_kind` DISABLE KEYS */;
INSERT INTO `clients_kind` VALUES (1,'Кот'),(2,'Пес');
/*!40000 ALTER TABLE `clients_kind` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clients_pet`
--

DROP TABLE IF EXISTS `clients_pet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clients_pet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `sex` tinyint(1) NOT NULL,
  `date_of_birth` date DEFAULT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `date_of_delete` varchar(20) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `kind_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `clients_pet_client_id_10fff7dd_fk_clients_client_id` (`client_id`),
  KEY `clients_pet_kind_id_ef26d6ba_fk_clients_kind_id` (`kind_id`),
  CONSTRAINT `clients_pet_client_id_10fff7dd_fk_clients_client_id` FOREIGN KEY (`client_id`) REFERENCES `clients_client` (`id`),
  CONSTRAINT `clients_pet_kind_id_ef26d6ba_fk_clients_kind_id` FOREIGN KEY (`kind_id`) REFERENCES `clients_kind` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clients_pet`
--

LOCK TABLES `clients_pet` WRITE;
/*!40000 ALTER TABLE `clients_pet` DISABLE KEYS */;
INSERT INTO `clients_pet` VALUES (1,'Шарик',1,NULL,'',NULL,1,2),(3,'qwergfv',1,NULL,'',NULL,1,1);
/*!40000 ALTER TABLE `clients_pet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `django_admin_log_chk_1` CHECK ((`action_flag` >= 0))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(3,'auth','group'),(2,'auth','permission'),(4,'auth','user'),(8,'clients','client'),(7,'clients','kind'),(9,'clients','pet'),(5,'contenttypes','contenttype'),(17,'filials','cabinet'),(16,'filials','filial'),(18,'schedule','schedule'),(15,'services','direction'),(14,'services','service'),(6,'sessions','session'),(10,'users','user'),(11,'visits','visit'),(12,'workers','position'),(13,'workers','worker');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2020-06-06 06:40:31.889754'),(2,'auth','0001_initial','2020-06-06 06:40:32.069274'),(3,'admin','0001_initial','2020-06-06 06:40:32.514087'),(4,'admin','0002_logentry_remove_auto_add','2020-06-06 06:40:32.630798'),(5,'admin','0003_logentry_add_action_flag_choices','2020-06-06 06:40:32.640747'),(6,'contenttypes','0002_remove_content_type_name','2020-06-06 06:40:32.754476'),(7,'auth','0002_alter_permission_name_max_length','2020-06-06 06:40:32.811291'),(8,'auth','0003_alter_user_email_max_length','2020-06-06 06:40:32.840247'),(9,'auth','0004_alter_user_username_opts','2020-06-06 06:40:32.850190'),(10,'auth','0005_alter_user_last_login_null','2020-06-06 06:40:32.900079'),(11,'auth','0006_require_contenttypes_0002','2020-06-06 06:40:32.904045'),(12,'auth','0007_alter_validators_add_error_messages','2020-06-06 06:40:32.913051'),(13,'auth','0008_alter_user_username_max_length','2020-06-06 06:40:32.970865'),(14,'auth','0009_alter_user_last_name_max_length','2020-06-06 06:40:33.031735'),(15,'auth','0010_alter_group_name_max_length','2020-06-06 06:40:33.050683'),(16,'auth','0011_update_proxy_permissions','2020-06-06 06:40:33.068603'),(17,'sessions','0001_initial','2020-06-06 06:40:33.088550'),(18,'users','0001_initial','2020-06-06 06:59:17.937043'),(19,'clients','0001_initial','2020-06-06 06:59:18.002868'),(20,'services','0001_initial','2020-06-06 06:59:18.214302'),(21,'filials','0001_initial','2020-06-06 06:59:18.418756'),(22,'workers','0001_initial','2020-06-06 06:59:18.654126'),(23,'schedule','0001_initial','2020-06-06 06:59:18.915429'),(24,'visits','0001_initial','2020-06-06 06:59:19.121031');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filials_cabinet`
--

DROP TABLE IF EXISTS `filials_cabinet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `filials_cabinet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` longtext NOT NULL,
  `state_work` tinyint(1) NOT NULL,
  `filial_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `filials_cabinet_filial_id_f126ddfa_fk_filials_filial_id` (`filial_id`),
  CONSTRAINT `filials_cabinet_filial_id_f126ddfa_fk_filials_filial_id` FOREIGN KEY (`filial_id`) REFERENCES `filials_filial` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filials_cabinet`
--

LOCK TABLES `filials_cabinet` WRITE;
/*!40000 ALTER TABLE `filials_cabinet` DISABLE KEYS */;
INSERT INTO `filials_cabinet` VALUES (1,'Смотровая','вапвапвап',1,1);
/*!40000 ALTER TABLE `filials_cabinet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filials_cabinet_services`
--

DROP TABLE IF EXISTS `filials_cabinet_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `filials_cabinet_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cabinet_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `filials_cabinet_services_cabinet_id_service_id_aa679e66_uniq` (`cabinet_id`,`service_id`),
  KEY `filials_cabinet_serv_service_id_9bd481db_fk_services_` (`service_id`),
  CONSTRAINT `filials_cabinet_serv_cabinet_id_d141e05e_fk_filials_c` FOREIGN KEY (`cabinet_id`) REFERENCES `filials_cabinet` (`id`),
  CONSTRAINT `filials_cabinet_serv_service_id_9bd481db_fk_services_` FOREIGN KEY (`service_id`) REFERENCES `services_service` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filials_cabinet_services`
--

LOCK TABLES `filials_cabinet_services` WRITE;
/*!40000 ALTER TABLE `filials_cabinet_services` DISABLE KEYS */;
/*!40000 ALTER TABLE `filials_cabinet_services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filials_filial`
--

DROP TABLE IF EXISTS `filials_filial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `filials_filial` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address_full` varchar(200) NOT NULL,
  `address` varchar(200) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `date_of_delete` varchar(20) DEFAULT NULL,
  `visibility` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filials_filial`
--

LOCK TABLES `filials_filial` WRITE;
/*!40000 ALTER TABLE `filials_filial` DISABLE KEYS */;
INSERT INTO `filials_filial` VALUES (1,'Иркутск, Советская 155','Советская 155','почта',NULL,1),(2,'Иркутск, Байкальская 155','Байкальская 155','почта',NULL,1);
/*!40000 ALTER TABLE `filials_filial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedule_schedule`
--

DROP TABLE IF EXISTS `schedule_schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `schedule_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time1_start` datetime(6) DEFAULT NULL,
  `time1_end` datetime(6) DEFAULT NULL,
  `time2_start` datetime(6) DEFAULT NULL,
  `time2_end` datetime(6) DEFAULT NULL,
  `cabinet_id` int(11) NOT NULL,
  `filial_id` int(11) NOT NULL,
  `worker_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `schedule_schedule_cabinet_id_e4a7d415_fk_filials_cabinet_id` (`cabinet_id`),
  KEY `schedule_schedule_filial_id_77029399_fk_filials_filial_id` (`filial_id`),
  KEY `schedule_schedule_worker_id_69ad61d0_fk_workers_worker_id` (`worker_id`),
  CONSTRAINT `schedule_schedule_cabinet_id_e4a7d415_fk_filials_cabinet_id` FOREIGN KEY (`cabinet_id`) REFERENCES `filials_cabinet` (`id`),
  CONSTRAINT `schedule_schedule_filial_id_77029399_fk_filials_filial_id` FOREIGN KEY (`filial_id`) REFERENCES `filials_filial` (`id`),
  CONSTRAINT `schedule_schedule_worker_id_69ad61d0_fk_workers_worker_id` FOREIGN KEY (`worker_id`) REFERENCES `workers_worker` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedule_schedule`
--

LOCK TABLES `schedule_schedule` WRITE;
/*!40000 ALTER TABLE `schedule_schedule` DISABLE KEYS */;
INSERT INTO `schedule_schedule` VALUES (1,'2020-06-07 08:00:00.000000','2020-06-07 12:00:00.000000',NULL,NULL,1,1,1),(2,'2020-06-08 08:00:00.000000','2020-06-08 12:00:00.000000',NULL,NULL,1,1,1);
/*!40000 ALTER TABLE `schedule_schedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services_direction`
--

DROP TABLE IF EXISTS `services_direction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `services_direction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `date_of_delete` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services_direction`
--

LOCK TABLES `services_direction` WRITE;
/*!40000 ALTER TABLE `services_direction` DISABLE KEYS */;
/*!40000 ALTER TABLE `services_direction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services_direction_services`
--

DROP TABLE IF EXISTS `services_direction_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `services_direction_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `direction_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `services_direction_servi_direction_id_service_id_39f1fcef_uniq` (`direction_id`,`service_id`),
  KEY `services_direction_s_service_id_911cc223_fk_services_` (`service_id`),
  CONSTRAINT `services_direction_s_direction_id_b5070965_fk_services_` FOREIGN KEY (`direction_id`) REFERENCES `services_direction` (`id`),
  CONSTRAINT `services_direction_s_service_id_911cc223_fk_services_` FOREIGN KEY (`service_id`) REFERENCES `services_service` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services_direction_services`
--

LOCK TABLES `services_direction_services` WRITE;
/*!40000 ALTER TABLE `services_direction_services` DISABLE KEYS */;
/*!40000 ALTER TABLE `services_direction_services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services_service`
--

DROP TABLE IF EXISTS `services_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `services_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` longtext NOT NULL,
  `duration` int(11) NOT NULL,
  `cost` double NOT NULL,
  `date_of_delete` varchar(20) DEFAULT NULL,
  `nurse` tinyint(1) NOT NULL,
  `visibility` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services_service`
--

LOCK TABLES `services_service` WRITE;
/*!40000 ALTER TABLE `services_service` DISABLE KEYS */;
INSERT INTO `services_service` VALUES (1,'услуга1','вапвапвап',40,200,NULL,0,1),(2,'услуга2','вкпавапп',20,200,NULL,0,1);
/*!40000 ALTER TABLE `services_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_user`
--

DROP TABLE IF EXISTS `users_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(150) DEFAULT NULL,
  `password` varchar(500) DEFAULT NULL,
  `token` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_user`
--

LOCK TABLES `users_user` WRITE;
/*!40000 ALTER TABLE `users_user` DISABLE KEYS */;
INSERT INTO `users_user` VALUES (1,'qw','qw','lGAs0RtAHEbhtbd0D6HYVHRYABurMo'),(3,'qwe','qwe','ARz6MG5F6m1yzMJQM5NlpIqY5BCEQb');
/*!40000 ALTER TABLE `users_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `visits_visit`
--

DROP TABLE IF EXISTS `visits_visit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `visits_visit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(20) DEFAULT NULL,
  `date` datetime(6) DEFAULT NULL,
  `comment` longtext NOT NULL,
  `cost` double NOT NULL,
  `status` varchar(20) DEFAULT NULL,
  `date_of_delete` varchar(20) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `pet_name` varchar(50) DEFAULT NULL,
  `cabinet_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `doctor_id` int(11) DEFAULT NULL,
  `filial_id` int(11) DEFAULT NULL,
  `pet_id` int(11) DEFAULT NULL,
  `pet_kind_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `visits_visit_cabinet_id_925d2914_fk_filials_cabinet_id` (`cabinet_id`),
  KEY `visits_visit_client_id_28388262_fk_clients_client_id` (`client_id`),
  KEY `visits_visit_doctor_id_ecb90376_fk_workers_worker_id` (`doctor_id`),
  KEY `visits_visit_filial_id_8ab645ff_fk_filials_filial_id` (`filial_id`),
  KEY `visits_visit_pet_id_fcf877b2_fk_clients_pet_id` (`pet_id`),
  KEY `visits_visit_pet_kind_id_00a1ab7b_fk_clients_kind_id` (`pet_kind_id`),
  CONSTRAINT `visits_visit_cabinet_id_925d2914_fk_filials_cabinet_id` FOREIGN KEY (`cabinet_id`) REFERENCES `filials_cabinet` (`id`),
  CONSTRAINT `visits_visit_client_id_28388262_fk_clients_client_id` FOREIGN KEY (`client_id`) REFERENCES `clients_client` (`id`),
  CONSTRAINT `visits_visit_doctor_id_ecb90376_fk_workers_worker_id` FOREIGN KEY (`doctor_id`) REFERENCES `workers_worker` (`id`),
  CONSTRAINT `visits_visit_filial_id_8ab645ff_fk_filials_filial_id` FOREIGN KEY (`filial_id`) REFERENCES `filials_filial` (`id`),
  CONSTRAINT `visits_visit_pet_id_fcf877b2_fk_clients_pet_id` FOREIGN KEY (`pet_id`) REFERENCES `clients_pet` (`id`),
  CONSTRAINT `visits_visit_pet_kind_id_00a1ab7b_fk_clients_kind_id` FOREIGN KEY (`pet_kind_id`) REFERENCES `clients_kind` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `visits_visit`
--

LOCK TABLES `visits_visit` WRITE;
/*!40000 ALTER TABLE `visits_visit` DISABLE KEYS */;
INSERT INTO `visits_visit` VALUES (1,NULL,'2020-06-07 12:00:00.000000','сасаиса',200,'2',NULL,20,NULL,NULL,1,1,1,1,1,1),(5,NULL,'2020-06-07 08:00:00.000000','',200,'0',NULL,40,NULL,NULL,1,1,1,1,1,NULL),(7,NULL,'2020-06-07 10:40:00.000000','',200,'0',NULL,40,NULL,NULL,1,1,1,1,1,NULL);
/*!40000 ALTER TABLE `visits_visit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `visits_visit_services`
--

DROP TABLE IF EXISTS `visits_visit_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `visits_visit_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `visit_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `visits_visit_services_visit_id_service_id_55554597_uniq` (`visit_id`,`service_id`),
  KEY `visits_visit_services_service_id_9f99abf3_fk_services_service_id` (`service_id`),
  CONSTRAINT `visits_visit_services_service_id_9f99abf3_fk_services_service_id` FOREIGN KEY (`service_id`) REFERENCES `services_service` (`id`),
  CONSTRAINT `visits_visit_services_visit_id_f68cd442_fk_visits_visit_id` FOREIGN KEY (`visit_id`) REFERENCES `visits_visit` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `visits_visit_services`
--

LOCK TABLES `visits_visit_services` WRITE;
/*!40000 ALTER TABLE `visits_visit_services` DISABLE KEYS */;
/*!40000 ALTER TABLE `visits_visit_services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workers_position`
--

DROP TABLE IF EXISTS `workers_position`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `workers_position` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workers_position`
--

LOCK TABLES `workers_position` WRITE;
/*!40000 ALTER TABLE `workers_position` DISABLE KEYS */;
INSERT INTO `workers_position` VALUES (1,'терапевт');
/*!40000 ALTER TABLE `workers_position` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workers_worker`
--

DROP TABLE IF EXISTS `workers_worker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `workers_worker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `surname` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `patronymic` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `visibility` tinyint(1) NOT NULL,
  `date_of_delete` varchar(20) DEFAULT NULL,
  `info` longtext NOT NULL,
  `position_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  KEY `workers_worker_position_id_6f29c0fb_fk_workers_position_id` (`position_id`),
  CONSTRAINT `workers_worker_position_id_6f29c0fb_fk_workers_position_id` FOREIGN KEY (`position_id`) REFERENCES `workers_position` (`id`),
  CONSTRAINT `workers_worker_user_id_387e1800_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workers_worker`
--

LOCK TABLES `workers_worker` WRITE;
/*!40000 ALTER TABLE `workers_worker` DISABLE KEYS */;
INSERT INTO `workers_worker` VALUES (1,'Петров','Петр','Петрович','234567','ывапр','вапроооо',NULL,1,NULL,'ыккпаверар',1,NULL);
/*!40000 ALTER TABLE `workers_worker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workers_worker_services`
--

DROP TABLE IF EXISTS `workers_worker_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `workers_worker_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `worker_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `workers_worker_services_worker_id_service_id_458ad41e_uniq` (`worker_id`,`service_id`),
  KEY `workers_worker_servi_service_id_29191232_fk_services_` (`service_id`),
  CONSTRAINT `workers_worker_servi_service_id_29191232_fk_services_` FOREIGN KEY (`service_id`) REFERENCES `services_service` (`id`),
  CONSTRAINT `workers_worker_services_worker_id_7868b4d3_fk_workers_worker_id` FOREIGN KEY (`worker_id`) REFERENCES `workers_worker` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workers_worker_services`
--

LOCK TABLES `workers_worker_services` WRITE;
/*!40000 ALTER TABLE `workers_worker_services` DISABLE KEYS */;
INSERT INTO `workers_worker_services` VALUES (1,1,1);
/*!40000 ALTER TABLE `workers_worker_services` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-06 21:30:18
