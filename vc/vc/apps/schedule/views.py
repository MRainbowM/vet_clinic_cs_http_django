from rest_framework.response import Response
from rest_framework.views import APIView

from .models import *
# from .serializers import *

from services.models import Service
from visits.models import Visit

import datetime

class ScheduleView(APIView):
    def get(self, request):
        params_str = request.META.get('QUERY_STRING')
        params = params_str.split('&')
        schedule_id = 0
        service_id = 0
        for param in params:
            if 'schedule_id' in param:
                schedule_id = param.split('=')[1]
            if 'service_id' in param:
                service_id = param.split('=')[1]

        
        if schedule_id != 0 and service_id != 0:
            try:
                service = Service.objects.get(id=service_id)
            except:
                return Response({
                    "status": "error",
                    "msg": "Неверный id услуги"})

            duration = service.duration
            try:
                schedule = Schedule.objects.get(id=schedule_id)
            except:
                return Response({
                    "status": "error",
                    "msg": "Неверный id расписания"})
            date = schedule.time1_start.date()
            visits_q = Visit.objects.filter(date__year=date.year, date__month=date.month, date__day=date.day).order_by('date')
            visits = {}
            for row in visits_q:
                date_end = row.date + datetime.timedelta(minutes = row.duration)
                visit = dict(
                    doctor_id = row.doctor_id,
                    date_start = row.date,
                    date_end = date_end)
                key = row.id
                visits[key] = visit

            times = {}
            time = {}
            dt = schedule.time1_start  
            i = 1
            
            for visit in visits:
                if visits[visit]["doctor_id"] == schedule.worker.id:
                    while dt < schedule.time1_end :
                        if dt >= visits[visit]["date_start"] and dt <= visits[visit]["date_end"]:
                            dt = visits[visit]["date_end"] + datetime.timedelta(minutes = 5) + datetime.timedelta(minutes = duration)
                            break
                        else: 
                            time["date_end"] = str(dt) #end
                            time["date_start"] = str(dt - datetime.timedelta(minutes = duration))
                            times[i] = time.copy()
                            i += 1
                            dt = dt + datetime.timedelta(minutes = 5)
            while dt <= schedule.time1_end:
                dt = dt + datetime.timedelta(minutes = 5) + datetime.timedelta(minutes = duration)
                if dt <= schedule.time1_end:
                    time["date_end"] = str(dt) #end
                    time["date_start"] = str(dt - datetime.timedelta(minutes = duration))
                    times[i] = time.copy()
                    i += 1

            if len(times) == 0:
                return Response({
                    "status": "error",
                    "msg": "Нет свободных дат",
                    "count":0})
            else:
                print(times)
                return Response({
                    "status": "success",
                    "content": times,
                    "count":len(times)})

        else:
            filial_id = 0
            worker_id = 0
            for param in params:
                if 'filial_id' in param:
                    filial_id = param.split('=')[1]
                if 'worker_id' in param:
                    worker_id = param.split('=')[1]

            if filial_id == 0 or worker_id == 0:
                return Response({
                    "status": "error",
                    "msg": "Необходимо передать id работника и id филиала"})

            today = datetime.datetime.now()
            schedules = Schedule.objects.filter(time1_end__year__gte=today.year, time1_end__month__gte=today.month, time1_end__day__gte=today.day)

            schedules_str = {}
            for schedule in schedules:
                d = {"date": schedule.time1_start.date()}
                schedules_str[schedule.id] = d

            return Response({
                    "status": "success",
                    "content": schedules_str,
                    "count":len(schedules)})
        return Response({
            "status": "error",
            "msg": "Не все данные переданы"})

            



