from django.urls import path
from .views import *

app_name = "schedule"

urlpatterns = [
    path('schedule/', ScheduleView.as_view()),
]