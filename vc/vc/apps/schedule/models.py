from django.db import models
from workers.models import Worker
from filials.models import Filial, Cabinet

class Schedule(models.Model):
    worker = models.ForeignKey(Worker, on_delete = models.PROTECT, verbose_name='Работник', blank=True, null=True)
    filial = models.ForeignKey(Filial, on_delete = models.PROTECT, verbose_name='Филиал')
    cabinet = models.ForeignKey(Cabinet, on_delete = models.PROTECT, verbose_name='Кабинет')
    time1_start = models.DateTimeField('Дата и время начала 1', null=True)
    time1_end = models.DateTimeField('Дата и время окончания 1', null=True)
    time2_start = models.DateTimeField('Дата и время начала 2', null=True)
    time2_end = models.DateTimeField('Дата и время окончания 2', null=True)

