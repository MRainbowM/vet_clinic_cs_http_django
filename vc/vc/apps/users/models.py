from django.db import models


class User(models.Model):
    username = models.CharField('Логин', max_length = 150, null=True, blank=True)
    password = models.CharField('Пароль', max_length = 500, null=True, blank=True)
    token = models.CharField('Телефон', max_length = 50, null=True, blank=True)
    
