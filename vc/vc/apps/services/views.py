from rest_framework.response import Response
from rest_framework.views import APIView


# from users.models import User

from .models import *
from .serializers import *


class ServiceView(APIView):
    def get(self, request, service_id=None):
        if service_id == None:
            services = Service.objects.filter(visibility=True, date_of_delete=None)
            services_str = {}
            for service in services:
                p = {
                    "id":service.id,
                    "name":service.name,
                    "description":service.description,
                    "duration":service.duration,
                    "cost":service.cost}
                services_str[service.id] = p
            
            return Response({
                "status": "success",
                "content": services_str,
                "count":len(services)})
        else:
            service = Service.objects.filter(id = service_id)
            if (len(service) == 0):
                return Response({
                    "status": "error",
                    "msg": "Ошибка: услуга не найдена"})
            else:
                service = {
                    "id":service.id,
                    "name":service.name,
                    "description":service.description,
                    "duration":service.duration,
                    "cost":service.cost}
                return Response({
                    "status": "success",
                    "content": service})


        

