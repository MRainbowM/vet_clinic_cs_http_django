from django.urls import path
from .views import *

app_name = "services"

urlpatterns = [
    path('services/', ServiceView.as_view()),
    path('services/<int:service_id>', ServiceView.as_view()),
]