from django.db import models
from django.core.validators import MinValueValidator
from workers.models import Worker
from clients.models import *
from filials.models import Filial, Cabinet
from services.models import Service
# from sales.models import Sale

class Visit(models.Model):
    client = models.ForeignKey(Client, on_delete = models.PROTECT, verbose_name='Клиент', blank=True, null=True)
    phone = models.CharField('Телефон', max_length = 20, null=True, blank=True)
    pet = models.ForeignKey(Pet, on_delete = models.PROTECT, verbose_name='Питомец', blank=True, null=True)
    doctor = models.ForeignKey(Worker, on_delete = models.PROTECT, verbose_name='Врач', blank=True, null=True)
    services = models.ManyToManyField(Service, blank=True, verbose_name='Услуги')
    # sale = models.ForeignKey(Sale, on_delete = models.PROTECT, null=True, blank=True, verbose_name='Акция')
    date = models.DateTimeField('Дата и время', null=True)
    filial = models.ForeignKey(Filial, on_delete = models.PROTECT, verbose_name='Филиал', null=True)
    cabinet = models.ForeignKey(Cabinet, on_delete = models.PROTECT, verbose_name='Кабинет', null=True)
    comment = models.TextField('Комментарий от врача', blank=True)
    cost = models.FloatField('Стоимость', validators=[MinValueValidator(0)])
    status = models.CharField('Статус', max_length = 20,null=True, blank=True)
    date_of_delete = models.CharField('Дата удаления', blank=True, max_length = 20, null=True)
    duration = models.IntegerField('Продолжительность', blank=True, null=True)
    name = models.CharField('Имя клиента (не зарег)', max_length = 50, null=True, blank=True)
    pet_kind = models.ForeignKey(Kind, on_delete = models.PROTECT, verbose_name='Вид животного (не зарег)', null=True, blank=True)
    pet_name = models.CharField('Кличка (не зарег)', max_length = 50, blank=True, null=True)

    # def __str__(self):
    #     return self.id

    class Meta:
        verbose_name = 'Прием'
        verbose_name_plural = 'Приемы'
