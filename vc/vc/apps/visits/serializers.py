from rest_framework.serializers import ModelSerializer

from .models import *

class VisitSerializer(ModelSerializer):
    class Meta:
        model = Visit
        fields = ['date', 'cost', 'duration']