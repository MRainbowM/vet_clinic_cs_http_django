# Generated by Django 3.0.7 on 2020-06-06 06:59

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('filials', '0001_initial'),
        ('services', '0001_initial'),
        ('workers', '0001_initial'),
        ('clients', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Visit',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('phone', models.CharField(blank=True, max_length=20, null=True, verbose_name='Телефон')),
                ('date', models.DateTimeField(null=True, verbose_name='Дата и время')),
                ('comment', models.TextField(blank=True, verbose_name='Комментарий от врача')),
                ('cost', models.FloatField(validators=[django.core.validators.MinValueValidator(0)], verbose_name='Стоимость')),
                ('status', models.CharField(blank=True, max_length=20, null=True, verbose_name='Статус')),
                ('date_of_delete', models.CharField(blank=True, max_length=20, null=True, verbose_name='Дата удаления')),
                ('duration', models.IntegerField(blank=True, null=True, verbose_name='Продолжительность')),
                ('name', models.CharField(blank=True, max_length=50, null=True, verbose_name='Имя клиента (не зарег)')),
                ('pet_name', models.CharField(blank=True, max_length=50, null=True, verbose_name='Кличка (не зарег)')),
                ('cabinet', models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='filials.Cabinet', verbose_name='Кабинет')),
                ('client', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='clients.Client', verbose_name='Клиент')),
                ('doctor', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='workers.Worker', verbose_name='Врач')),
                ('filial', models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='filials.Filial', verbose_name='Филиал')),
                ('pet', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='clients.Pet', verbose_name='Питомец')),
                ('pet_kind', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='clients.Kind', verbose_name='Вид животного (не зарег)')),
                ('services', models.ManyToManyField(blank=True, to='services.Service', verbose_name='Услуги')),
            ],
            options={
                'verbose_name': 'Прием',
                'verbose_name_plural': 'Приемы',
            },
        ),
    ]
