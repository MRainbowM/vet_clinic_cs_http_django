from rest_framework.response import Response
from rest_framework.views import APIView

from .models import *
# from .serializers import *
import datetime

from users.models import User
from clients.models import Client, Pet
from filials.models import Cabinet
from schedule.models import Schedule

class VisitView(APIView):
    def get(self, request):
        token = request.META.get('HTTP_TOKEN')
        try:
            user = User.objects.get(token=token)
            client = Client.objects.get(user=user)
        except:
            return Response({
                "status": "error",
                "msg": "Ошибка авторизации"})
        params_str = request.META.get('QUERY_STRING')
        params = params_str.split('&')
        pet_id = 0
        for param in params:
            if 'pet_id' in param:
                pet_id = param.split('=')[1]

        if pet_id != 0:
            visits = Visit.objects.filter(client=client, pet_id=pet_id)
        elif pet_id == 0 :
            visits = Visit.objects.filter(client=client)
        try:
            if len(visits) == 0:
                return Response({
                    "status": "error",
                    "msg": "Приемы не найдены"})
            else:
                visits_str = {}
                for visit in visits:
                    status = ""
                    if visit.status == '0':
                        status = "актуальный"
                    if visit.status == '1':
                        status = "выполнен"
                    if visit.status == '2':
                        status = "отменен"
                    v = {
                        "id":visit.id,
                        "date":visit.date,
                        "status":status,
                        "cabinet":visit.cabinet.name,
                        "duration":visit.duration,
                        "filial":visit.filial.address,
                        "pet":visit.pet.name,
                        "doctor":visit.doctor.surname + ' '+ visit.doctor.name,
                    }
                    key = "visit " + str(visit.id)
                    visits_str[key] = v
                return Response({
                    "status": "success",
                    "content": visits_str,
                    "count": len(visits)})
        except:
            pass
            return Response({
                "status": "error",
                "msg": "Приемы не найдены"})

    def post(self, request):              
        token = request.META.get('HTTP_TOKEN')
        try:
            user = User.objects.get(token=token)
            client = Client.objects.get(user=user)
        except:
            return Response({
                "status": "error",
                "msg": "Ошибка авторизации"})
        service_id = request.data.get('service_id')
        filial_id = request.data.get('filial_id')
        date = request.data.get('date')
        pet_id = request.data.get('pet_id')
        worker_id = request.data.get('worker_id')
        if (service_id == None or service_id =='' or
            worker_id == None or worker_id =='' or
            pet_id == None or pet_id == '' or 
            date == None or date == '' or
            filial_id == None or filial_id == ''):
            return Response({
                "status": "error",
                "msg": "Не все данные переданы"})
        try:
            service = Service.objects.get(id=service_id)
        except:
            return Response({
                "status": "error",
                "msg": "Неверный id услуги"})
        
        cost = service.cost
        duration = service.duration

        workers = Worker.objects.filter(services=service)
        if len(workers) == 0:
            return Response({
                "status": "error",
                "msg": "Неверный id услуги"})

        try:
            worker = Worker.objects.get(id=worker_id)
        except:
            return Response({
                "status": "error",
                "msg": "Неверный id врача"})

        if not worker in workers:
            return Response({
                "status": "error",
                "msg": "Ошибка, работник не оказывает данную услугу"})

        try:
            pet = Pet.objects.get(id=pet_id, client=client)
        except:
            return Response({
                "status": "error",
                "msg": "Неверный id питомца"})

        date = datetime.datetime.strptime(date[0:18], "%Y-%m-%d %H:%M:%S")
        
        try:
            cabinet = Schedule.objects.get(filial_id=filial_id, time1_start__year=date.year,
            time1_start__month=date.month, time1_start__day=date.day, worker_id=worker_id).cabinet
        except:
            return Response({
                "status": "error",
                "msg": "Ошибка, кабинет не найден"})

        new_visit = Visit.objects.create(
            date = date,
            cost = cost,
            filial_id = filial_id,
            doctor_id = worker_id,
            pet_id = pet_id,
            client_id = client.id,
            status = 0,
            duration = duration,
            cabinet_id = cabinet.id
        )

        visit = {
            "id":new_visit.id,
            "date":new_visit.date,
            "status":"актуальный",
            "cabinet":new_visit.cabinet.name,
            "duration":new_visit.duration,
            "filial":new_visit.filial.address,
            "pet":new_visit.pet.name,
            "doctor":new_visit.doctor.surname + ' '+ new_visit.doctor.name,
        }

        return Response({
                "status": "success",
                "msg": visit})

    def delete(self, request, visit_id): 
        token = request.META.get('HTTP_TOKEN')
        try:
            user = User.objects.get(token=token)
            client = Client.objects.get(user=user)
        except:
            return Response({
                "status": "error",
                "msg": "Ошибка авторизации"})

        try:
            visit = Visit.objects.get(id=visit_id)
        except:
            return Response({
                "status": "error",
                "msg": "Неверный id приема"})
        visit.status = 2
        visit.save()
        return Response({
            "status": "success",
            "msg": "Прием отменен"})


    