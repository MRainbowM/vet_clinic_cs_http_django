from django.urls import path
from .views import *

app_name = "visits"

urlpatterns = [
    path('visits/', VisitView.as_view()),
    path('visits/<int:visit_id>', VisitView.as_view()),
]