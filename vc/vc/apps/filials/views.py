from rest_framework.response import Response
from rest_framework.views import APIView



from .models import Cabinet, Filial
from .serializers import FilialSerializer


class FilialView(APIView):
    def get(self, request):
        filials = Filial.objects.filter(date_of_delete=None,visibility=True)
        if len(filials) == 0:
            return Response({
                "status": "error",
                "msg": "Филилалы не найдены",
                "count":0})
        else:
            filials_str = {}
            for filial in filials:
                p = {
                    "id":filial.id,
                    "address_full":filial.address_full,
                    "address":filial.address,
                    "mail":filial.mail}
                filials_str[filial.id] = p


            return Response({
                "status": "success",
                "content": filials_str,
                "count":len(filials)})













# from schedule.models import Schedule
# # import random
# # import string

# class CabinetView(APIView):
#     def get(self, request, service_id=None):
#         if service_id != None:
#             service = Service.objects.get()
#             cabinets = Cabinet.objects.filter(service=)

# class ServiceView(APIView):
#     def get(self, request, service_id=None):
#         if service_id == None:
#             services = Service.objects.filter(visibility=True, date_of_delete=None)
#             serializer = ServiceSerializer(services, many=True)
#             return Response({
#                 "status": "success",
#                 "content": serializer.data,
#                 "count":len(services)})
#         else:
#             service = Service.objects.filter(id = service_id)
#             if (len(service) == 0):
#                 return Response({
#                     "status": "error",
#                     "msg": "Ошибка: услуга не найдена"})
#             else:
#                 serializer = ServiceSerializer(service, many=True)
#                 return Response({
#                 "status": "success",
#                 "content": serializer.data})