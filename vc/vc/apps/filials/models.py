from django.db import models
from django.core.validators import MinValueValidator
from services.models import Service



class Filial(models.Model):
    address_full = models.CharField('Полный адрес', max_length = 200)
    address = models.CharField('Короткий адрес', max_length = 200)
    mail = models.CharField('Почта', max_length = 50)
    date_of_delete = models.CharField('Дата удаления', blank=True, max_length = 20, null=True)
    visibility = models.BooleanField('Видимость на сайте', default=True)
    class Meta:
        verbose_name = 'Филиал'
        verbose_name_plural = 'Филиалы'

class Cabinet(models.Model):
    name = models.CharField('Название', max_length = 50)
    description = models.TextField('Описание', blank=True)
    state_work = models.BooleanField('Статус работы', default=True)
    services = models.ManyToManyField(Service, blank=True, verbose_name='Услуги в кабинете')
    filial = models.ForeignKey(Filial, on_delete = models.CASCADE, verbose_name='Филиал', blank=True)

    class Meta:
        verbose_name = 'Кабинет'
        verbose_name_plural = 'Кабинеты'    




