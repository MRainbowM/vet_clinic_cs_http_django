from rest_framework.serializers import ModelSerializer

from .models import *

# class CabinetSerializer(ModelSerializer):
#     class Meta:
#         model = Cabinet
#         fields = ['id', 'surname', 'name']
        
class FilialSerializer(ModelSerializer):
    class Meta:
        model = Filial
        fields = ['address']