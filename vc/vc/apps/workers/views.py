from rest_framework.response import Response
from rest_framework.views import APIView

from .models import *
from .serializers import *

from services.models import Service

class WorkerView(APIView):
    def get(self, request):
        params_str = request.META.get('QUERY_STRING')
        params = params_str.split('&')
        service_id = 0
        for param in params:
            if 'service_id' in param:
                service_id = param.split('=')[1]
        if service_id != 0:
            try:
                service = Service.objects.get(id=service_id)
            except:
                return Response({
                    "status": "error",
                    "msg": "Неверный id услуги"})
            workers = Worker.objects.filter(services=service)
            if len(workers) == 0:
                return Response({
                    "status": "error",
                    "msg": "Врачи не найдены",
                    "count": 0})
            else:
                workers_str = {}
                for worker in workers:
                    p = {
                        "id":worker.id,
                        "surname":worker.surname,
                        "name":worker.name,
                        "patronymic":worker.patronymic,
                        "phone":worker.phone,
                        "mail":worker.mail,
                        "date_of_birth":worker.date_of_birth,
                        "info":worker.info}
                    workers_str[worker.id] = p

                return Response({
                    "status": "success",
                    "content": workers_str,
                    "count": len(workers)})

