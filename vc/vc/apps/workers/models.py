from django.db import models
from django.core.validators import MinValueValidator
from services.models import Service
from django.contrib.auth.models import User

class Position(models.Model):
    name = models.CharField('Название должности', max_length = 50)
    
    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name = 'Должность'
        verbose_name_plural = 'Должности'

class Worker(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True)
    surname = models.CharField('Фамилия', max_length = 50)
    name = models.CharField('Имя', max_length = 50)
    patronymic = models.CharField('Отчество', max_length = 50)
    phone = models.CharField('Телефон', max_length = 20)
    mail = models.CharField('Электронная почта', max_length = 50)
    photo = models.ImageField('Фото', null=True, blank=True, upload_to="upload/")
    date_of_birth = models.DateField('Дата рождения', null=True)
    visibility = models.BooleanField('Видимость на сайте', default=True)
    date_of_delete = models.CharField('Дата удаления', blank=True, max_length = 20, null=True)
    services = models.ManyToManyField(Service, verbose_name='Услуги')
    position = models.ForeignKey(Position, on_delete = models.PROTECT, verbose_name='Должность')
    info = models.TextField('Информация на сайте', blank=True) 

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Работник'
        verbose_name_plural = 'Работники'


