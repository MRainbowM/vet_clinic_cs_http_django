from rest_framework.serializers import ModelSerializer

from .models import *

class WorkerSerializer(ModelSerializer):
    class Meta:
        model = Worker
        fields = ['id', 'surname', 'name', 'patronymic']