from django.db import models
from django.core.validators import MinValueValidator
# from django.contrib.auth.models import User

from django.apps import apps

# User = apps.get_model('users', 'User')

class Kind(models.Model):
    value = models.CharField('Вид', max_length = 50)
    
    def __str__(self):
        return self.value
    
    class Meta:
        verbose_name = 'Вид животного'
        verbose_name_plural = 'Виды животных'

class Client(models.Model):
    user = models.ForeignKey('users.User',on_delete=models.CASCADE, null=True, blank=True)
    # user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True)
    surname = models.CharField('Фамилия', max_length = 50, null=True, blank=True)
    name = models.CharField('Имя', max_length = 50, null=True, blank=True)
    patronymic = models.CharField('Отчество', max_length = 50, null=True, blank=True)
    phone = models.CharField('Телефон', max_length = 20, null=True, blank=True)
    mail = models.CharField('Электронная почта', max_length = 50, null=True, blank=True)
    photo = models.ImageField('Фото', null=True, blank=True, upload_to="upload/")
    date_of_birth = models.DateField('Дата рождения', null=True, blank=True)
    date_of_delete = models.CharField('Дата удаления', blank=True, max_length = 20, null=True)
    

    
    def __str__(self):
        if self.name:
            return self.name
        else:
            return str(self.id)

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'

class Pet(models.Model):
    name = models.CharField('Кличка', max_length = 50, null=True)
    sex = models.BooleanField('Пол (true - m)', default=True)
    kind = models.ForeignKey(Kind, on_delete = models.PROTECT, verbose_name='Вид животного', null=True)
    date_of_birth = models.DateField('Дата рождения', null=True)
    photo = models.ImageField('Фото', null=True, blank=True, upload_to="upload/")
    client = models.ForeignKey(Client, on_delete = models.CASCADE, verbose_name='Клиент', blank=True, null=True)
    date_of_delete = models.CharField('Дата удаления', blank=True, max_length = 20, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Питомец'
        verbose_name_plural = 'Питомцы'





        
