from rest_framework.response import Response
from rest_framework.views import APIView


from users.models import User
from users.serializers import UserSerializer

from .models import Client, Pet, Kind
from .serializers import *

import random
import string

class ClientAuthView(APIView):
    def post(self, request):
        login = request.data.get('login')
        pwd = request.data.get('password')
        if login == None or pwd == None:
            return Response({
                "status": "error",
                "msg": "Введите логин и пароль!"})
        try:
            user = User.objects.get(username=login, password=pwd)
        except:
            return Response({
                "status": "error",
                "msg": "Неверный логин или пароль!"})

        token = ""
        token = ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(30))
        
        user.token = token
        user.save()
       
        return Response({"status": "succes",
            "content":{"token": token}})

class ClientView(APIView):
    def get(self, request):
        token = request.META.get('HTTP_TOKEN')
        try:
            user = User.objects.get(token=token)
            client = Client.objects.get(user=user)
        except:
            return Response({
                "status": "error",
                "msg": "Ошибка авторизации"})
        client = {
            "id" : client.id,
            "surname" : client.surname,
            "name" : client.name,
            "patronymic" : client.patronymic,
            "phone" : client.phone,
            "mail" : client.mail,
            "date_of_birth" : client.date_of_birth,
            "user_id" : client.user_id}
        return Response({"status":"success", "content":client})

    def post(self, request):
        print(request.data)
        login = request.data.get('login')
        pwd = request.data.get('password')
        try:
           user = User.objects.get(username=login)
           return Response({"status":"error", "msg":"Данный логин занят"})
        except:
            user = User.objects.create(username=login, password=pwd)
        try:
            client = Client.objects.create(user=user)
            return Response({"status":"success", "content": {"client_id":client.id}})
        except: 
            return Response({"status":"error", "msg":"Клиент не создан"})


class PetView(APIView):
    def post(self, request):
        token = request.META.get('HTTP_TOKEN')
        pet_name = request.data.get('pet_name')
        pet_kind_id = request.data.get('pet_kind_id')
        pet_sex = request.data.get('pet_sex')
        try:
            user = User.objects.get(token=token)
            client = Client.objects.get(user=user)
        except:
            return Response({
                "status": "error",
                "msg": "Ошибка авторизации"})

        try:
            pet = Pet.objects.create(name=pet_name, sex=pet_sex, kind_id=pet_kind_id, client=client)
            return Response({"status":"success", "content": {"pet_id":pet.id}})
        except: 
            return Response({"status":"error", "msg":"Питомец не создан"})

    def get(self, request):
        token = request.META.get('HTTP_TOKEN')
        try:
            user = User.objects.get(token=token)
            client = Client.objects.get(user=user)
        except:
            return Response({
                "status": "error",
                "msg": "Ошибка авторизации"})
        try:
            pets = Pet.objects.filter(client=client)
            if len(pets) > 0:
                pets_str = {}
                for pet in pets:
                    p = {
                       "id":pet.id,
                       "name":pet.name,
                       "sex":pet.sex,
                       "kind":pet.kind.value,
                       "date_of_birth":pet.date_of_birth
                    }
                    pets_str[pet.id] = p


                return Response({"status":"success", 
                    "content": pets_str,
                    "count":len(pets)})
            else:
                return Response({
                "status": "error",
                "msg": "Питомцы не найдены",
                "count": 0})

        except: 
            return Response({
                "status": "error",
                "msg": "Ошибка"})




class KindView(APIView):
    def get(self, request):
        kinds = Kind.objects.filter().all()
        if len(kinds) == 0:
            return Response({
                "status": "error",
                "msg": "Виды животных не найдены",
                "count":0})
        else:
            kinds_str = {}
            for kind in kinds:
                p = {
                    "id":kind.id,
                    "value":kind.value}
                kinds_str[kind.id] = p
            return Response({
                "status": "success",
                "content": kinds_str,
                "count":len(kinds)})

        


