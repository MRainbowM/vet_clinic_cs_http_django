from django.urls import path
from .views import *

app_name = "clients"

urlpatterns = [
    path('auth/', ClientAuthView.as_view()),
    path('client/', ClientView.as_view()),
    path('pets/', PetView.as_view()),
    path('kinds/', KindView.as_view()),
    
]