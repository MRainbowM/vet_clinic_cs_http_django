from rest_framework.serializers import ModelSerializer

from .models import *

class ClientSerializer(ModelSerializer):
    class Meta:
        model = Client
        fields = ['id', 'surname', 'name']

class KindSerializer(ModelSerializer):
    class Meta:
        model = Kind
        fields = ['value']


class PetSerializer(ModelSerializer):
    class Meta:
        model = Pet
        fields = ['id', 'name', 'sex']
